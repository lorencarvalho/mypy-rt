#!/usr/bin/env python
# vim:set fileencoding=utf8:

from setuptools import setup, find_packages
from setuptools.extension import Extension
from Cython.Build import cythonize

with open('README.mkd') as readme_file:
    readme = readme_file.read()

requirements = []

setup(
    name='mypy-rt',
    version='0.1.0',
    description="Fast runtime mypy-based type-checking",
    long_description=readme,
    author="William Orr",
    author_email='will@worrbase.com',
    url='https://gitlab.com/worr/mypy-rt',
    packages=find_packages(),
    setup_requires=["Cython"],
    ext_modules=cythonize(
        [Extension("mypyrt.mypyrt", ["./mypyrt/mypyrt.pyx"])]),
    include_package_data=True,
    install_requires=requirements,
    license="MIT",
    zip_safe=False,
    keywords='mypy-rt',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
    ], )
